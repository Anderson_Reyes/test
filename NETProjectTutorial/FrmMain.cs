﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtTablas;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionExtintores fgp = new FrmGestionExtintores();
            fgp.MdiParent = this;
            fgp.DsCel = dsCel;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtTablas = dsCel.Tables["Extintor"];
            ExtintorModel.Populate();
            foreach (Extintor p in ExtintorModel.GetProductos())
            {
                dtTablas.Rows.Add(p.Id, p.marca, p.tipo, p.med, p.capa, p.cat, p.Lugar, p.Lugar);
            }


           ClienteModel.Populate();
            foreach (Cliente c in ClienteModel.GetListEmpleado())
            {
                dsCel.Tables["Cliente"].Rows.Add(c.Id, c.Cedula, c.Nombre, c.Apellidos, c.Direccion,c.Tcelular, c.Nombre, c.Apellidos);
            }
        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("En proceso");
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmFactura ff = new FrmFactura();
            //ff.MdiParent = this;
            //ff.DsSistema = dsCel;
            //ff.Show();
        }
    }
}
