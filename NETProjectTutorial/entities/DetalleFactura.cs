﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class DetalleFactura
    {
        private int id;
        private Factura factura;
        private Extintor producto;
        private int cantidad;
        private double precio;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        internal Factura Factura
        {
            get
            {
                return factura;
            }

            set
            {
                factura = value;
            }
        }

        internal Extintor Producto
        {
            get
            {
                return producto;
            }

            set
            {
                producto = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public DetalleFactura(int id, Factura factura, Extintor producto, int cantidad, double precio)
        {
            this.Id = id;
            this.Factura = factura;
            this.Producto = producto;
            this.Cantidad = cantidad;
            this.Precio = precio;
        }

     


    }
}
