﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Cliente
    {
        private int id;
        private string nombre;
        private string apellidos;
        private string cedula;
        private string direccion;
        private string correo;
        private string municipio;
        private string departamento;
        private string tcelular;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Municipio
        {
            get
            {
                return municipio;
            }

            set
            {
                municipio = value;
            }
        }

        public string Departamento
        {
            get
            {
                return departamento;
            }

            set
            {
                departamento = value;
            }
        }

        public string Tcelular
        {
            get
            {
                return tcelular;
            }

            set
            {
                tcelular = value;
            }
        }


        public Cliente() { }

        public Cliente(int id, string nombre, string apellidos, string cedula, string direccion, string correo, string municipio, string departamento, string tcelular)
        {
            this.id = id;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.cedula = cedula;
            this.direccion = direccion;
            this.correo = correo;
            this.municipio = municipio;
            this.departamento = departamento;
            this.tcelular = tcelular;
        }


       


       
        //public override string ToString()
        //{
        //    return  Nombre + " " + Apellidos;
        //}



    }
}
