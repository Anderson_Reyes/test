﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Extintor
    {
        private int id;
        private MARCA Marca;
        private TIPO Tipo;
        private CATEGORIA categoria;

        private CAPACIDAD capacidad;

        private MEDIDA u_medida;

        private DateTime fecharecarga;

        private string lugar;

        public Extintor(int id, MARCA marca, TIPO tipo, CATEGORIA categoria, CAPACIDAD capacidad, MEDIDA u_medida, DateTime fecharecarga, string lugar)
        {
            this.Id = id;
            Marca = marca;
            Tipo = tipo;
            this.categoria = categoria;
            this.capacidad = capacidad;
            this.u_medida = u_medida;
            this.fecharecarga = fecharecarga;
            this.Lugar = lugar;
        }

        internal MARCA marca
        {
            get
            {
                return marca;
            }
            set
            {
                marca = value;
            }
        }
        internal CATEGORIA cat
        {
            get
            {
                return cat;
            }
            set
            {
                cat = value;
            }
        }

        internal TIPO tipo
        {
            get
            {
                return tipo;
            }
            set
            {
                tipo = value;
            }
        }

        internal MEDIDA med
        {
            get
            {
                return med;
            }
            set
            {
                med = value;
            }
        }
        internal CAPACIDAD capa
        {
            get
            {
                return capa;
            }
            set
            {
                capa = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Lugar
        {
            get
            {
                return lugar;
            }

            set
            {
                lugar = value;
            }
        }

        public enum MARCA
        {
            TORNADO, AMEREX, OTRO
        }

        public enum MEDIDA
        {
            LITROS, LIBRAS
        }

        public enum CATEGORIA
        {
            AMERICANO, EUROPEO
        }

        public enum TIPO
        {
           CO2, AGUA, ESPUMA 
        }

        public enum CAPACIDAD
        {
            U_5, U_10, U_20, U_50
        }

    }
}

