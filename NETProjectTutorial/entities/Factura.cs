﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Factura
    {
        private int id;
        private string cod_factura;
        private DateTime fecha;
        private Cliente empleado;
        private string observaciones;
        private double subtotal;
        private double iva;
        private double total;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        internal Cliente Empleado
        {
            get
            {
                return empleado;
            }

            set
            {
                empleado = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public Factura(int id, string cod_factura, DateTime fecha, Cliente empleado, string observaciones, double subtotal, double iva, double total)
        {
            this.Id = id;
            this.Cod_factura = cod_factura;
            this.Fecha = fecha;
            this.Empleado = empleado;
            this.Observaciones = observaciones;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
        }

     
    }
}
