﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.Properties;
using NETProjectTutorial.entities;

namespace NETProjectTutorial
{
    public partial class FrmExtintor : Form
    {
        private DataTable tblTablas ;
        private DataSet dsCel;
        private BindingSource bsCel;
        private DataRow drCel;

        public FrmExtintor()
        {
            InitializeComponent();
            bsCel = new BindingSource();
        }

       
        public DataRow DrProducto
        {
            set {
                drCel = value;
                cmbMarca.Text =  drCel["Marca"].ToString();
                cmbTipo.Text = drCel["Tipo"].ToString();
                cmbMed.Text = drCel["Medida"].ToString();
                cmbCat.Text = drCel["Categoria"].ToString(); 
                cmbCap.Text = drCel["Capacidad"].ToString(); ; ;
               
            }
            
        }

        public DataTable TblTablas
        {
            get
            {
                return tblTablas;
            }

            set
            {
                tblTablas = value;
            }
        }

        public DataSet DsCel
        {
            get
            {
                return dsCel;
            }

            set
            {
                dsCel = value;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string capa = cmbCap.Text;
            string marca = cmbCap.Text;

            
            //if (drCel != null)
            //{
            //    DataRow drNew = TblTablas.NewRow();

            //    int index = TblTablas.Rows.IndexOf(drCel);
            //   // drNew["Id"] = drCel["Id"];
            //    drNew["Marca"] = marca;
            //    drNew["Capacidad"] = capa;
            //    drNew["Categoria"] = cat;


            //    TblTablas.Rows.RemoveAt(index);
            //    TblTablas.Rows.InsertAt(drNew, index);               

            //}
            

            Dispose();

        }

        private void FrmProducto_Load(object sender, EventArgs e)
        {
            bsCel.DataSource = DsCel;
            bsCel.DataMember = DsCel.Tables["Extintor"].TableName;       
             

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
