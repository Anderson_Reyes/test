﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionExtintores : Form
    {
        private DataSet dsCel;
        private BindingSource bsCel;

        public DataSet DsCel
        {
            get
            {
                return dsCel;
            }

            set
            {
                dsCel = value;
            }
        }

        public FrmGestionExtintores()
        {
            InitializeComponent();
            bsCel = new BindingSource();
        }

   
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsCel.Filter = string.Format("Marca like '*{0}*' or Medida like '*{0}*' or Tipo like '*{0}*' ", textBox1.Text);
               
            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmExtintor fp = new FrmExtintor();
            fp.TblTablas = DsCel.Tables["Extintor"];
            fp.DsCel = DsCel;
            fp.ShowDialog();
        }

        private void FrmGestionProductos_Load(object sender, EventArgs e)
        {
            bsCel.DataSource = DsCel;
            bsCel.DataMember = DsCel.Tables["Extintor"].TableName;
            dataGridView1.DataSource = bsCel;
            dataGridView1.AutoGenerateColumns = true;

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if(rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow =  rowCollection[0];
            DataRow drow = ((DataRowView) gridRow.DataBoundItem).Row;

            FrmExtintor fp = new FrmExtintor();
            fp.TblTablas = DsCel.Tables["Extintor"];
            fp.DsCel = DsCel;
            fp.DrProducto = drow;
            fp.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dataGridView1.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result =  MessageBox.Show(this,"Realmente desea eliminar ese registro?","Mensaje del Sistema",MessageBoxButtons.YesNo,MessageBoxIcon.Question);

            if(result == DialogResult.Yes)
            {
                DsCel.Tables["Extintor"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }
    }
}
