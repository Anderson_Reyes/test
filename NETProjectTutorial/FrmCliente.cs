﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblCel2;
        private DataSet dsCel2;
        private BindingSource bsEmpleados;
        private DataRow drCel2;

        public FrmCliente()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public DataRow DrCel2
        {
            set
            {
                drCel2 = value;
                //txtINSS.Text = drCel2["INSS"].ToString();
                txtCed.Text = drCel2["Cédula"].ToString();
                txtNombres.Text = drCel2["Nombres"].ToString();
                txtApellidos.Text = drCel2["Apellidos"].ToString();
                txtDireccion.Text = drCel2["Dirección"].ToString();
                //txtConvencional.Text = drCel2["Teléfono"].ToString();
                txtCelular.Text = drCel2["Celular"].ToString();
                txtDireccion.Text = drCel2["Dirección"].ToString();
                //cmbSexo.SelectedItem = drCel2["Sexo"].ToString();
            }
        }

        public DataTable TblCel2
        {
            get
            {
                return tblCel2;
            }

            set
            {
                tblCel2 = value;
            }
        }

        public DataSet DsCel2
        {
            get
            {
                return dsCel2;
            }

            set
            {
                dsCel2 = value;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string cedula, correo, nombres, apellidos, direccion, tCelular, sexo;
            //double salario;

            correo = txtCorreo.Text;
            cedula = txtCed.Text;
            nombres = txtNombres.Text;
            apellidos = txtApellidos.Text;
            direccion = txtDireccion.Text;
            //tConvencional = txtConvencional.Text;
            tCelular = txtCelular.Text;
            //salario = double.Parse(txtSalario.Text);
           // sexo = cmbSexo.SelectedItem.ToString();
            

            if (drCel2 != null)
            {
                DataRow drNew = TblCel2.NewRow();

                int index = TblCel2.Rows.IndexOf(drCel2);
                drNew["Id"] = drCel2["Id"];
                drNew["Correo"] = correo;
                drNew["Cedula"] = cedula;
                drNew["Nombres"] = nombres;
                drNew["Apellidos"] = apellidos;
                drNew["Direccion"] = direccion;
                //drNew["Convencional"] = tConvencional;
                drNew["Celular"] = tCelular;
               // drNew["Salario"] = salario;
                //drNew["Sexo"] = sexo;


                TblCel2.Rows.RemoveAt(index);
                TblCel2.Rows.InsertAt(drNew, index);

            }
            else
            {
                TblCel2.Rows.Add(TblCel2.Rows.Count + 1, correo, cedula, nombres, apellidos, direccion, tCelular);
            }

            Dispose();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = DsCel2;
            bsEmpleados.DataMember = DsCel2.Tables["Cliente"].TableName;
        }
    }
}
